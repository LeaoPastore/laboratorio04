# Laboratorio04

Laboratório 4 - Sistema em Tempo Real e Embarcados - Menção 1

# Descrição do Projeto

O circuito em questão tem a função de, ao detectar uma incidencia de luz menor que um certo nível, o circuito deve disparar um alarme.
Neste circuito, a porta A0 irá receber a informação enviada pelo sensor luminoso, essa informação será utilizada pelo sistema
e caso o valor seja menor que 850 será transmitido através da porta 13 um sinal de valor 1 que irá acionar o componente sonoro do sistema
caso o valor recebido seja maior que 850 será transmitido o valor 0 deixando o componente sonoro em pausa.
O valor recebido pela porta A0 esta a todo o tempo sendo printado no monitor, para que se possa acompanhar o comportamente do sistema.

# Código

void setup(){
  Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(13, OUTPUT);
}

void loop(){
  int r = analogRead(A0);
  Serial.println(r);
  if (r < 850){
  	digitalWrite(13, HIGH);
  }
  else{
  	digitalWrite(13, LOW);
  }
}

# Link para o projeto 

https://www.tinkercad.com/things/71n8uSTZDVl-super-kasi-bigery/editel?sharecode=CAesUWcQU0wTONCt-vWpcDbyluswebdQoAZFT4Slayw